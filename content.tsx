import type { PlasmoCSConfig } from "plasmo"
import createCache from "@emotion/cache"
import { CacheProvider } from "@emotion/react"
import { ThemeProvider } from "@mui/material/styles";
import { MyTheme } from "~utils/types";
import ContentPage from "~components/content";

const styleElement = document.createElement("style")
const styleCache = createCache({
  key: "plasmo-mui-cache",
  prepend: true,
  container: styleElement
})

export const getStyle = () => styleElement

export const config: PlasmoCSConfig = {
  matches: ["*://*.amazon.de/*", "*://*.amazon.com/*"]
}

function IndexPopup() {
  return (
    <CacheProvider value={styleCache}>
      <ThemeProvider theme={MyTheme}>
        <ContentPage />
      </ThemeProvider>
    </CacheProvider>
  )
}

export default IndexPopup
