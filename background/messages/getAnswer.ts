import type { PlasmoMessaging } from "@plasmohq/messaging"
import { firebaseUrl, getDoc, wait } from "~background/utils";

const handler: PlasmoMessaging.MessageHandler = async (req, res) => {
    try {
        console.log('Getting Answer for message...')
        console.log("getAnswer body: ", req.body)
        const msgId: string = req.body.id

        while (true) {
            const chatMessage = await getDoc(`${firebaseUrl}${msgId}`);
            console.log("Answer Check: ", chatMessage.data);
            if (chatMessage.data.fields.answer) return res.send({
                answer: chatMessage.data.fields.answer.stringValue
            })
            await wait(2);
        }
    } catch (error) {
        console.log(error)
        res.send({
            error: error.toString()
        })
    }
}

export default handler
