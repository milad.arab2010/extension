import type { PlasmoMessaging } from "@plasmohq/messaging"
import { Storage } from "@plasmohq/storage"
import {
    TypingString,
    type Chat,
    type ChatHistoryMessage,
    type ChatMessage,
    type ProductInfo,
    type ProductQa,
    type ProductReview,
    type ProductData,
    getStringValue,
    getBooleanValue,
    localStorage
} from "../../utils/types"
import {
    addDoc,
    firebaseDocumentsUrl,
    firebaseUrl,
    getDoc,
    setDoc
} from "~background/utils"

const handler: PlasmoMessaging.MessageHandler = async (req, res) => {
    const storage = localStorage

    try {
        console.log('Start prompting...')
        console.log("req body: ", req.body)
        const chat: Chat = req.body.chat;
        const chatMessage: ChatMessage = req.body.chatMessage
        let chatId: string = req.body.chatId
        const productId: string = req.body.productId
        const productDataId: string = req.body.productDataId
        const productData: ProductData = await storage.get(productDataId)
        const productAdded = await upsertProduct(productId, productData.info)
        if (productAdded === true) {
            await addProductQa(productId, productData.productQa)
            await addProductReviews(productId, productData.productReviews)
        }
        chatId = await upsertChat(chatId, chat)
        console.log("addChat resp: ", chatId)
        const messageData: ChatHistoryMessage = await prompt(chatId, chatMessage)

        res.send({
            chatId,
            messageData
        });
    } catch (error) {
        console.log(error)
        res.send({
            error: error.toString()
        })
    }
}

export default handler

async function upsertChat(chatId: string, chat: Chat) {
    const chatPath = `${firebaseDocumentsUrl}chats/`
    let chatDoc = await getDoc(`${firebaseUrl}${chatId}`)

    console.log("Upsert Chat data: ", chatDoc)

    if (chatDoc.exists && !chatDoc.data.fields.closed.booleanValue) {
        console.log("Chat exists and is open: ", chatDoc.data)
        return chatId
    }

    console.log("Chat doesn't exist or is closed: ", chatDoc.exists)
    chatDoc = await addDoc(chatPath, {
        fields: {
            customerId: chat.customerId,
            productId: chat.productId,
            uid: chat.uid,
            closed: chat.closed
        }
    });
    console.log("Chat added: ", chatDoc.data.name);
    return chatDoc.data.name
}

async function prompt(chatId: string, chatMessage: ChatMessage): Promise<ChatHistoryMessage> {
    const chatMessagePath = `${firebaseUrl}${chatId}/messages`
    const chatMessageDoc = await addDoc(chatMessagePath, {
        fields: {
            question: chatMessage.question,
            answered: chatMessage.answered
        }
    })
    console.log("Message created: ", chatMessageDoc.data.name)

    return {
        id: chatMessageDoc.data.name,
        createTime: chatMessageDoc.data.createTime,
        question: chatMessage.question.stringValue,
        answered: chatMessage.answered.booleanValue,
        answer: TypingString
    }
}

async function upsertProduct(productId: string, productInfo: ProductInfo): Promise<boolean> {
    const productPath = `${firebaseDocumentsUrl}products/${productId}`
    let productDoc = await getDoc(productPath);
    if (productDoc.exists) {
        console.log("Product exists in products table: ", productDoc.exists)
        return false
    }

    const productSummaryPath = `${firebaseDocumentsUrl}productsSummary/${productId}`
    const productSummaryDoc = await getDoc(productSummaryPath);
    if (productSummaryDoc.exists) {
        console.log("Product exists in productsSummary table: ", productDoc.exists)
        return false
    }

    productDoc = await setDoc(productPath, {
        fields: {
            info: {
                mapValue: {
                    fields: {
                        name: productInfo.name,
                        description: productInfo.description,
                    }
                }
            },
            summarized: getBooleanValue(false),
            id: getStringValue(productId)
        }
    });
    console.log("Product added: ", productDoc.data.name)
    return true
}

async function addProductQa(productId: string, productQa: Array<ProductQa>) {
    if (!Array.isArray(productQa)) return
    const productQaPath = `${firebaseDocumentsUrl}products/${productId}/qas`
    return Promise.all(productQa.map((value) => addDoc(productQaPath, {
        fields: {
            ...value
        }
    })))
}

async function addProductReviews(productId: string, productReviews: Array<ProductReview>) {
    if (!Array.isArray(productReviews)) return
    const productReviewsPath = `${firebaseDocumentsUrl}products/${productId}/reviews`
    return Promise.all(productReviews.map((value) => addDoc(productReviewsPath, {
        fields: {
            ...value
        }
    })))
}
