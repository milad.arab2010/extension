import { Storage } from "@plasmohq/storage"
import { app, auth } from "~firebase"
import {
    getFirestore,
} from "firebase/firestore"

const storage = new Storage();
const firestore = getFirestore(app);
const projectId = firestore.app.options.projectId
export const firebaseUrl = "https://firestore.googleapis.com/v1/"
export const firebaseDocumentsUrl = `${firebaseUrl}projects/${projectId}/databases/(default)/documents/`

export async function getDoc(path: string) {
    return betterFetch(path, {})
}

export async function setDoc(path: string, data) {
    return betterFetch(path, {
        body: data,
        method: "PATCH"
    })
}

export async function addDoc(path: string, data) {
    return betterFetch(path, {
        body: data,
        method: "POST"
    })
}

export async function wait(seconds: number) {
    await new Promise((resolve, reject) => setTimeout(() => {
        resolve(null)
    }, seconds * 1000))
}

export async function betterFetch(path: string, options) {
    const token = await storage.get("token")

    if (!token) {
        storage.set('user', null)
        throw new Error("Token is not provided")
    }

    options.headers = new Headers({
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    })
    if (options.body) {
        options.body = JSON.stringify(options.body)
    }

    const response = await fetch(`${path}`, options)
    if (!response.ok) {
        if (response.status === 404) return {
            exists: false,
            data: null
        }

        if (response.status === 401) {
            storage.set('token', null)
            storage.set('user', null)
            throw new Error(`Authentication fail.`);
        }

        console.log("Fetch Error: ", response)
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
    return {
        exists: true,
        data: await response.json()
    }
}
