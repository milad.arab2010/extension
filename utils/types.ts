import { createTheme } from "@mui/material/styles"
import { Storage } from "@plasmohq/storage"

export type Chat = {
    id: StringValue
    customerId: StringValue
    uid: StringValue
    productId: StringValue
    closed: BooleanValue
}

export type ChatMessage = {
    question: StringValue
    answered: BooleanValue
}

export type ProductInfo = {
    name: StringValue
    description: StringValue
}

export type StringValue = {
    stringValue: string
}

export type BooleanValue = {
    booleanValue: boolean
}

export type IntegerValue = {
    integerValue: number
}

export function getStringValue(value: string): StringValue {
    return {
        stringValue: value
    }
}

export function getBooleanValue(value: boolean): BooleanValue {
    return {
        booleanValue: value
    }
}

export function getIntegerValue(value: number): IntegerValue {
    return {
        integerValue: value
    }
}

export const MyTheme = createTheme({
    palette: {
        mode: 'light',
        primary: {
            main: '#8342E8',
        },
        secondary: {
            main: '#e842a8',
        },
    },
});

export type ChatHistoryMessage = {
    question: string,
    answer: string,
    id: string,
    answered: boolean,
    createTime: string,
}

export type ChatHistory = {
    id: string,
    messages: Array<ChatHistoryMessage>,
    recentQuestionId: string,
}

export const TypingString: string = "Typing..."

export type ProductQa = {
    question: StringValue
    answer: StringValue
}

export type ProductReview = {
    rating: IntegerValue
    comment: StringValue
}

export type ProductData = {
    productQa: Array<ProductQa>
    productReviews: Array<ProductReview>
    info: ProductInfo
}

export function displayLog(): void {
    console.log()
}

const lclStorage = new Storage({
    area: "local"
})

export const localStorage = lclStorage