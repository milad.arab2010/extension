export function getAsin(url): string{
    try {
        const dpProductRegExp = /(?:\/dp\/)(.*?)(?:\/|\?)/i
        const dpProductRegExpMatch = url.match(dpProductRegExp)
        const asin = dpProductRegExpMatch && dpProductRegExpMatch[1]
        console.log("Product Id was found: ", asin)
        return asin
    } catch (e) {
        console.log("Product Id was not found: ", e)
        return null;
    }
}

export function getLanguage(baseUrl): string{
    const landDomainRegExp = /(?<=\.)([a-z]+)(?:\/)/i
    const landDomainMatch = baseUrl.match(landDomainRegExp)
    const countryDomain = landDomainMatch && landDomainMatch[1]
    const languageCodesMap = {
        "de": "de_DE",
        "com": "en_US",
        "default": "en_US",
    }

    if (languageCodesMap[countryDomain]) return languageCodesMap[countryDomain]
    return languageCodesMap["default"]
}

export function getBaseUrl(url): string{
    const regexPattern = /^.*?\/\/.*?\//ig;
    const matches = url.match(regexPattern);
    const result = matches && matches[0];
    console.log("BaseUrl found: ", result)
    if (!result) throw new Error("getBaseUrl error") 
    return result
}

export function getProductDescription(): string {
    try {
        const descriptionElement = document.querySelector('#poExpander');
        const productDescriptionTable = descriptionElement.getElementsByTagName("tbody")[0];
        return getFormattedTextContent(productDescriptionTable)
    } catch (e) {
        console.log("Failed to get product description: ", e)
        return "";
    }
}

export function getProductInformation(): string {
    try {
        const informationElement = document.querySelector('#feature-bullets');
        return getFormattedTextContent(informationElement)
    } catch (e) {
        console.log("Failed to get product information: ", e)
        return "";
    }
}

export function getProductName(): string {
    try {
        const titleElement = document.querySelector('#title');
        const titleElementSpan = titleElement.getElementsByTagName("span")[0];
        return getFormattedTextContent(titleElementSpan)
    } catch (e) {
        console.log("Failed to get product name: ", e)
        return "";
    }
}

export function getFormattedTextContent(domElement) : string {
    return domElement.textContent?.trim().replace(/\s+/g, ' ').replace(/\n/g, '')
}

export function getTechSpecTable() : string {
    try {
        const dataTable = document.querySelector('#productDetails_techSpec_section_1');
        const tableBody = dataTable.getElementsByTagName("tbody")[0];
        return getFormattedTextContent(tableBody)
    } catch (e) {
        console.log("Failed to get product TechSpecTable: ", e)
        return "";
    }
}