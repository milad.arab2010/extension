import { getAsin, getBaseUrl, getFormattedTextContent, getLanguage } from "./data_collection_basic";
import {
    getStringValue,
    type ProductQa
} from "./types";

export async function getProductQa(): Promise<Array<ProductQa>> {
    try {
        const qa = []
        const currentUrl = window.location.href
        console.log(currentUrl)
        const baseUrl = getBaseUrl(currentUrl)
        const asin = getAsin(currentUrl)
        const language = getLanguage(baseUrl)
        let page = 1
        const qaUrl = `${baseUrl}ask/questions/asin/${asin}/${page}?sort=HELPFUL&isAnswered=TRUE&askLanguage=${language}`
        console.log(qaUrl)
        const resp = await fetch(qaUrl)
        if (!resp.ok) throw new Error("Error during obtaining product QA")
        const respText = await resp.text()
        const parser = new DOMParser();
        const doc = parser.parseFromString(respText, 'text/html')
        const questionNodes = doc.querySelectorAll('[id^="question"]')
        console.log("qn", questionNodes)

        questionNodes.forEach((questionNode) => {
            const answerNode = questionNode.nextElementSibling
            qa.push({
                question: getStringValue(getQuestion(questionNode)),
                answer: getStringValue(getAnswer(answerNode))
            })
        })

        function getQuestion(questionNode): string {
            const aLement = questionNode.querySelector('a')
            return aLement ? getFormattedTextContent(aLement) : ""
        }

        function getAnswer(answerNode): string {
            const aSpans = answerNode.querySelectorAll('span')
            return aSpans.length < 2 ? "" : getFormattedTextContent(aSpans[1])
        }

        console.log("qa table", qa)

        return qa
    } catch (e) {
        console.log("Failed to get Question/Answers: ", e)
        return [];
    }
}
