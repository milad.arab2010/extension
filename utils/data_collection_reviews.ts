import { getAsin, getBaseUrl, getFormattedTextContent, getLanguage } from "./data_collection_basic";
import {
    getIntegerValue,
    getStringValue,
    type ProductReview
} from "./types";

export async function getProductReviews(): Promise<Array<ProductReview>> {
    try {
        const reviews = []
        const currentUrl = window.location.href
        console.log(currentUrl)
        const baseUrl = getBaseUrl(currentUrl)
        const asin = getAsin(currentUrl)
        let page = 1
        const reviewsUrl = `${baseUrl}product-reviews/${asin}` ///reviewerType=all_reviews&pageNumber${page}
        console.log(reviewsUrl)
        const resp = await fetch(reviewsUrl)
        if (!resp.ok) throw new Error("Error during obtaining product QA")
        const respText = await resp.text()
        const parser = new DOMParser();
        const doc = parser.parseFromString(respText, 'text/html')
        const reviewNodes = doc.querySelectorAll('[id^="customer_review"]')
        console.log("rn", reviewNodes)

        reviewNodes.forEach((reviewNode) => {
            reviews.push({
                rating: getIntegerValue(getStars(reviewNode)),
                comment: getStringValue(getReview(reviewNode))
            })
        })
        
        function getStars(reviewNode): number {
            const starsNode = reviewNode.querySelector('i[data-hook="review-star-rating"]')
            const nodeClasses = Array.from(starsNode.classList)
            const starString = nodeClasses.find(className => /^a-star-/gi.test(className.toString()))?.toString().replace('a-star-','')
            return starString ? parseInt(starString) : 0
        }

        function getReview(reviewNode): string {
            const rSpan = reviewNode.querySelector('span[data-hook="review-body"]')
            return rSpan ? getFormattedTextContent(rSpan) : ""
        }

        console.log("reviews table", reviews)

        return reviews
    } catch (e) {
        console.log("Failed to get product information: ", e)
        return [];
    }
}
