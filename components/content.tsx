import { useStorage } from "@plasmohq/storage/hook";
import { sendToBackground } from "@plasmohq/messaging"
import {
    type User
} from "firebase/auth"
import { useTheme } from "@mui/material/styles";
import BuyWiseLogo from "data-base64:~assets/icon.png"
import {
    Avatar,
    Button,
    Card,
    Fab,
    Fade,
    Grow,
    IconButton,
    Stack,
    Typography
} from "@mui/material";
import ChatComponent from "~components/chat";
import { Close } from "@mui/icons-material";
import { useEffect } from "react";
import { getAsin } from "~utils/data_collection_basic";

function ContentPage() {
    const [user] = useStorage<User>("user");
    const [minified, setMinified] = useStorage<boolean>("BuyWiseMinified");
    const currentUrl = window.location.href;
    const productId = getAsin(currentUrl);
    const theme = useTheme();

    if (!productId) return;

    async function onLogin() {
        const resp = await sendToBackground({
            name: "login",
            body: {}
        })
    }

    async function toggleOpen(e) {
        await setMinified(!minified)
    }

    useEffect(() => {
        //verifyLogin()
    }, []);

    return <>
        <Grow 
            in={minified}
            style={{ transformOrigin: '75% 75%' }}>
            <Card elevation={24}
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    position: "fixed",
                    right: "30px",
                    bottom: "30px",
                    maxWidth: "400px",
                    width: "400px",
                    borderRadius: 3
                }}>
                <Stack
                    direction={"row"}
                    spacing={3}
                    sx={{
                        paddingBottom: 2,
                        pt: 2,
                        pl: 2,
                        pr: 2,
                        height: 35,
                        backgroundColor: theme.palette.primary.main,
                        color: theme.palette.primary.contrastText
                    }}
                    alignItems={'center'}
                    justifyContent="space-between">
                    <Avatar
                        alt="Buy Wise!"
                        src={BuyWiseLogo}
                        sx={{
                            borderColor: "white",
                            border: 2
                        }} />
                    <Typography>BuyWise</Typography>
                    <IconButton
                        color="inherit"
                        onClick={(e) => toggleOpen(e)}>
                        <Close></Close>
                    </IconButton>
                </Stack>
                {!user ? (
                    <>
                        <Button onClick={() => onLogin()}>Login</Button>
                    </>
                ) : (
                    <ChatComponent user={user} productId={productId}></ChatComponent>
                )}
            </Card>
        </Grow>
        <Fade in={!minified}>
            <Fab
                color="primary"
                onClick={(e) => toggleOpen(e)}
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    position: "fixed",
                    right: "30px",
                    bottom: "30px",
                    height: 64,
                    width: 64,
                }}>
                <img
                    src={BuyWiseLogo}
                    alt="BuyWiseLogo"
                    width={64}
                    height={64} />
            </Fab>
        </Fade>
    </>
}

export default ContentPage
