import { IconButton, Stack, TextField } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import {
    getStringValue,
    type ChatMessage,
    type ProductInfo,
    type Chat,
    getBooleanValue,
    type ChatHistory,
    type ProductQa,
    type ProductReview,
    type ProductData,
    localStorage
} from "~utils/types";
import MessagesComponent from "./messages";
import { Send } from '@mui/icons-material';
import type { User } from "firebase/auth";
import { sendToBackground } from "@plasmohq/messaging";
import { useStorage } from "@plasmohq/storage/hook";
import {
    getProductDescription,
    getProductInformation,
    getProductName,
    getTechSpecTable
} from "~utils/data_collection_basic";
import { getProductQa } from "~utils/data_collection_qa";
import { getProductReviews } from "~utils/data_collection_reviews";

interface Props {
    user: User,
    productId: string
}

function ChatComponent({ user, productId }: Props) {
    const chatId = productId + "_" + user.uid
    const productDataId = `${productId}_${user.uid}_productData`
    const [chatHistory, setChatHistory] = useStorage<ChatHistory>({
        key: chatId,
        instance: localStorage
      })
    //const [productData, setProductData] = useStorage<ProductData>(productDataId)
    const [productData, setProductData] = useStorage<ProductData>({
        key: productDataId,
        instance: localStorage
      })
    const [promptError, setPromptError] = useState<string>(null)
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [waiting, setWaiting] = useState<boolean>(false)
    const inputRef = useRef(null);

    async function askBot(e) {
        await waitForResponse()
        setIsLoading(true)
        setPromptError(null)
        try {
            const question = inputRef.current.value
            const minCharLimit = 10;
            const maxCharLimit = 150
            if (question.length < minCharLimit) throw new Error(`Question must have more than ${minCharLimit} characters!`)
            if (question.length > maxCharLimit) throw new Error(`Question must have less than ${maxCharLimit} characters!`)

            console.log("Question: ", question)

            const chat: Chat = {
                customerId: getStringValue(user.email),
                uid: getStringValue(user.uid),
                id: getStringValue(chatId),
                productId: getStringValue(productId),
                closed: getBooleanValue(false)
            }
            console.log("chat: ", chat)
            const chatMessage: ChatMessage = {
                question: getStringValue(question),
                answered: getBooleanValue(false)
            }

            console.log("Prompt: ", chatMessage)
            const chatHistoryTmp: ChatHistory = chatHistory || {
                id: "NonValidChatId",
                recentQuestionId: null,
                messages: []
            }

            const resp = await sendToBackground({
                name: "prompt",
                body: {
                    chatId: chatHistoryTmp.id,
                    chat,
                    chatMessage,
                    user,
                    productId,
                    productDataId
                }
            })
            console.log("prompt response: ", resp)
            if (resp.error) throw resp.error
            chatHistoryTmp.id = resp.chatId
            chatHistoryTmp.recentQuestionId = resp.messageData.id
            chatHistoryTmp.messages.push(resp.messageData)
            console.log("done processing, saving chat data: ", chatHistoryTmp)
            await setChatHistory(chatHistoryTmp)
            inputRef.current.value = ""
        } catch (error) {
            console.log("Error: ", error)
            setPromptError(error.toString())
        }
        setIsLoading(false)
        setWaiting(!waiting)
    }

    useEffect(() => {
        console.log("triggered product data collection")
        collectProductData()
    }, [])

    useEffect(() => {
        console.log("triggering wait for response: ", chatHistory)
        waitForResponse()
    }, [waiting, chatHistory])

    async function collectProductData(): Promise<void> {
        if (productData) return
        console.log("collecting product data start...")
        const info: ProductInfo = {
            name: getStringValue(getProductName()),
            description: getStringValue(getProductDescription() + " " + getProductInformation() + " " + getTechSpecTable())
        }

        const productQa: Array<ProductQa> = await getProductQa()
        console.log("Obtained QA: ", productQa)
        const productReviews: Array<ProductReview> = await getProductReviews()
        console.log("Obtained reviews: ", productReviews)

        setProductData({
            productQa,
            productReviews,
            info
        })

        console.log("collecting product data stop...")
    }

    async function waitForResponse() {
        console.log("Wait for response triggered: ", chatHistory)
        if (!chatHistory?.recentQuestionId) return
        setIsLoading(true)
        console.log("ask about answer... ", chatHistory)
        const resp = await sendToBackground({
            name: "getAnswer",
            body: {
                id: chatHistory.recentQuestionId
            }
        })
        console.log("Answer obtained! ", resp)
        const msgIndex = chatHistory.messages.findIndex((value) => value.id == chatHistory.recentQuestionId)
        chatHistory.messages[msgIndex].answer = resp?.answer || resp?.error || "Error: technical issue."
        chatHistory.messages[msgIndex].answered = true
        chatHistory.recentQuestionId = null
        console.log("ChatHistory processed", chatHistory)
        await setChatHistory(chatHistory)
        setIsLoading(false)
    }

    function handleInputKeyDown(e) {
        if (e.key !== 'Enter') return
        askBot(e)
    }

    return (
        <Stack
            spacing={1}>
            <MessagesComponent chat={chatHistory?.messages}></MessagesComponent>
            <Stack
                direction="row"
                spacing={1}
                sx={{
                    pb: 2,
                    pr: 2,
                    pl: 2,
                }}
                justifyContent="space-between"
                alignItems="center">
                <TextField
                    inputRef={inputRef}
                    label="Ask me anything about this product..."
                    variant="standard"
                    multiline
                    error={promptError ? true : false}
                    helperText={promptError ? promptError : " "}
                    disabled={isLoading ? true : false}
                    fullWidth
                    maxRows={3}
                    onKeyDown={(e) => handleInputKeyDown(e)}
                />
                <IconButton
                    color="primary"
                    onClick={(e) => askBot(e)}
                    disabled={isLoading ? true : false}>
                    <Send />
                </IconButton >
            </Stack>
        </Stack >
    )
}

export default ChatComponent