import { Avatar, Box, Button, CircularProgress, Container, Stack, ThemeProvider, Typography, useTheme } from "@mui/material"
import { useState } from "react";
import { useFirebase } from "~firebase/hook"
import BuyWiseLogo from "data-base64:~assets/icon.png"
import { Send, Sync } from "@mui/icons-material";

export default function OptionsInside() {
    const { user, isLoading, onLogin, onLogout } = useFirebase()
    const [operationError, setOperationError] = useState<string>(null);

    return (
        <Stack
            sx={{
                height: '90vh'
            }}
            spacing={2}
            alignItems="center"
            justifyContent="center">
            <Avatar
                sx={{ width: 64, height: 64 }} 
                alt="Buy Wise!" 
                src={BuyWiseLogo} />
            <Typography>
                BuyWise {user && <>, {user.email}</>}
            </Typography>
            {!user ? (
                <Button
                    variant="contained"
                    onClick={() => onLogin()}
                    endIcon={isLoading ? <Sync /> : <Send />}
                    disabled={isLoading ? true : false}>Sign In With Google</Button>
            ) : (
                <Button
                    variant="contained"
                    onClick={() => onLogout()}
                    endIcon={isLoading ? <Sync /> : <Send />}
                    disabled={isLoading ? true : false}>Log out</Button>
            )}
            {isLoading && (
                <Box
                    position="absolute"
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    top={0}
                    left={0}
                    bottom={0}
                    right={0}
                    bgcolor="rgba(0, 0, 0, 0.2)"
                >
                    <CircularProgress size={64} />
                </Box>
            )}
        </Stack>
    )
}