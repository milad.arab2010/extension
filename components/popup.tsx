import { useFirebase } from "~firebase/hook"
import { sendToBackground } from "@plasmohq/messaging"
import { useTheme } from "@mui/material/styles";
import { Avatar, Box, Button, Chip, CircularProgress, Divider, Stack, Typography } from "@mui/material"
import BuyWiseLogo from "data-base64:~assets/icon.png"
import DiscordLogo from "data-base64:~assets/discordIcon.png"
import { Send, Sync } from "@mui/icons-material";

export default function BuyWisePopup() {
    const { user, isLoading } = useFirebase()
    const theme = useTheme();
    async function onLogin() {
        const resp = await sendToBackground({
            name: "login",
            body: {}
        })
    }

    return (
        <Stack
            sx={{
                width: 300
            }}>
            <Stack
                direction="row"
                alignItems="center"
                justifyContent="space-between"
                sx={{
                    color: theme.palette.primary.main,
                    p: 1,
                    pb: 2,
                    height: 35
                }}>
                <Stack
                    direction="row"
                    spacing={1}>
                    <Avatar
                        sx={{
                            width: 24,
                            height: 24
                        }}
                        alt="Buy Wise!"
                        src={BuyWiseLogo} />
                    <Typography>
                        BuyWise Extension
                    </Typography>
                </Stack>
                <Chip
                    color="primary"
                    label="Alpha"
                    variant="outlined" />
            </Stack>
            <Divider light />
            <Stack
                justifyContent="center"
                alignItems="center"
                spacing={4}
                sx={{
                    pt: 6,
                    pb: 6,
                    pr: 2,
                    pl: 2
                }}>
                <Typography>Make Better Amazon Purchases</Typography>
                {!user ? (
                    <Button
                        variant="outlined"
                        onClick={() => onLogin()}
                        endIcon={isLoading ? <Sync /> : <Send />}
                        disabled={isLoading ? true : false}>Login</Button>
                ) : (
                    <Button
                        variant="outlined"
                        onClick={() => onLogin()}
                        endIcon={isLoading ? <Sync /> : <Send />}
                        disabled={isLoading ? true : false}>Options</Button>
                )}
                {isLoading && (
                    <Box
                        position="absolute"
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        top={0}
                        left={0}
                        bottom={0}
                        right={0}
                        bgcolor="rgba(0, 0, 0, 0.2)"
                    >
                        <CircularProgress size={64} />
                    </Box>
                )}
            </Stack>
            <Divider light />
            <Stack
                direction="row"
                justifyContent="center"
                alignItems="center"
                sx={{ p: 1, pt: 2 }}>
                <Chip
                    avatar={<Avatar alt="Discord" src={DiscordLogo} />}
                    label="Join Us"
                    variant="outlined"
                    clickable
                />
            </Stack>
        </Stack>
    )
}
