import { useTheme } from "@mui/material/styles";
import { Box, Paper, Typography } from "@mui/material";

function QuestionComponent({ question }) {
    const theme = useTheme();
    console.log("adding question: ", question)

    return (
        <Box sx={{
            pl: 6
        }}>
            <Paper
                elevation={2}
                sx={{
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.primary.contrastText,
                    pt: 1,
                    pb: 1,
                    pl: 2,
                    pr: 2,
                    borderRadius: 5
                }}>
                <Typography
                    sx={{
                        wordWrap: "break-word"
                    }}>{question}</Typography>
            </Paper>
        </Box>
    )
}

export default QuestionComponent