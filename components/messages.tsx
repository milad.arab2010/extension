import { Stack } from "@mui/material";
import { useEffect, useRef } from "react";
import QuestionComponent from "./question";
import AnswerComponent from "./answer";

function MessagesComponent({ chat }) {
    const stackRef = useRef(null)
    const items = []
    const bgImage = chrome.runtime.getURL('assets/chatBg.png')

    if (!Array.isArray(chat) || chat.length < 1) {
        chat = []
    }

    chat.forEach(message => {
        items.push(<QuestionComponent key={message.createTime} question={message.question} />)
        items.push(<AnswerComponent key={'i' + message.createTime} answer={message.answer} />)
    });

    useEffect(() => {
        if (stackRef.current) {
            stackRef.current.scrollTop = stackRef.current.scrollHeight;
        }
    });

    return (
        <Stack
            ref={stackRef}
            spacing={2}
            sx={{
                maxHeight: 350,
                height: 350,
                overflow: 'auto',
                p: 2,
                backgroundImage: `url(${bgImage})`,
                backgroundSize: 'cover',
                backgroundRepeat: 'repeat'
            }}>
            {items.map((item) => {
                return item
            })}
        </Stack>
    )
}

export default MessagesComponent