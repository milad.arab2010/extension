import { CircularProgress, Paper, Stack, Typography } from "@mui/material";
import { TypingString } from "~utils/types";

function AnswerComponent({ answer }) {
    console.log("adding answer: ", answer)
    return (
        <Paper
            elevation={2}
            sx={{
                width: '80%',
                pt: 1,
                pb: 1,
                pl: 2,
                pr: 2,
                borderRadius: 5
            }}>
            <Stack
                direction="row"
                spacing={1}
                alignItems={'center'}>
                {answer === TypingString && <CircularProgress size={24}/>}
                <Typography
                    sx={{
                        wordWrap: "break-word"
                    }}>{answer}</Typography>
            </Stack>

        </Paper>
    )
}

export default AnswerComponent