import { ThemeProvider } from "@mui/material/styles"
import OptionsInside from "~components/options"
import { MyTheme } from "~utils/types"

export default function OptionsPopup() {
  return (
      <ThemeProvider theme={MyTheme}>
        <OptionsInside />
      </ThemeProvider>
  )
}
