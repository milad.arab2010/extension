import { ThemeProvider } from "@mui/material/styles"
import BuyWisePopup from "~components/popup"
import { MyTheme } from "~utils/types"

export default function IndexPopup() {
  return (
      <ThemeProvider theme={MyTheme}>
        <BuyWisePopup />
      </ThemeProvider>
  )
}
